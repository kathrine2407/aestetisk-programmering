function setup() {
  // put setup code here
createCanvas(1500, 800); 
 background(255);
}

function draw() {
  // put drawing code here

  //Jeg laver to cirkler som danner min emoji
  noStroke();
  fill(220);
  circle(400,335,380); //cirkel 1
  circle(1010,335,380); //cirkel 2

  // Jeg laver 6 forskellige firkanter med hver deres farve så man selv kan vælge farve på cirklen ud fra valgmulighederne. 
  
  fill(255,153,255); // lyserød
  noStroke();
  rect(230,600,100,100,20);
 
  fill(204,255,153); // grøn
  noStroke();
  rect(350,600,100,100,20);

  fill(51,204,255); // blå
  noStroke();
  rect(470,600,100,100,20);

  fill(255,204,153); //beige
  noStroke();
  rect(840,600,100,100,20);

  fill(204,153,102); //sand
  noStroke();
  rect(960,600,100,100,20);

  fill(153,102,0); //brun
  noStroke();
  rect(1080,600,100,100,20);

 
  push();
  // Ansigtet på cirkel 2 
  fill(220);
  ellipse(820, 320, 60, 95); //øverste del af øret (venstre)
  ellipse(815, 345, 40, 65); //nederste del af øret (venstre)
  ellipse(1195, 320, 60, 95); //øverste del af øret (højre)
  ellipse(1200, 345, 40, 65);//nederste del af øret (højre)
  pop();
  
  // Jeg foretager nu if statements for at cirklerne (emojis) ændre farve ift placering af mus i firkanterne

  // Disse if statments er for smiley 1 
if(mouseX>=230 && mouseX<=330 && mouseY>=600 && mouseY<=700){
  fill(255,153,255);
  circle(400,335,380);// lyserød firkant
}
  if(mouseX>=350 && mouseX<=450 && mouseY>=600 && mouseY<=700){
  fill(204,255,153); // grøn firkant
  circle(400,335,380);
}
if(mouseX>=470 && mouseX<=570 && mouseY>=600 && mouseY<=700){
  fill(51,204,255); // blå firkant
  circle(400,335,380);
}
  // Disse if statments er for smiley 2
if(mouseX>=840 && mouseX<=940 && mouseY>=600 && mouseY<=700){
  fill(255,204,153); // beige firkant
  circle(1010,335,380);
  ellipse(820, 320, 60, 95); //øverste del af øret (venstre)
  ellipse(815, 345, 40, 65); //nederste del af øret (venstre)
  ellipse(1195, 320, 60, 95); //øverste del af øret (højre)
  ellipse(1200, 345, 40, 65);//nederste del af øret (højre)

}
if(mouseX>=960 && mouseX<=1060 && mouseY>=600 && mouseY<=700){
  fill(204,153,102); // sand firkant
  circle(1010,335,380);
  ellipse(820, 320, 60, 95); //øverste del af øret (venstre)
  ellipse(815, 345, 40, 65); //nederste del af øret (venstre)
  ellipse(1195, 320, 60, 95); //øverste del af øret (højre)
  ellipse(1200, 345, 40, 65);//nederste del af øret (højre)
}
if(mouseX>=1080 && mouseX<=1180 && mouseY>=600 && mouseY<=700){
  fill(153,102,0); // brun firkant 
  circle(1010,335,380);
  ellipse(820, 320, 60, 95); //øverste del af øret (venstre)
  ellipse(815, 345, 40, 65); //nederste del af øret (venstre)
  ellipse(1195, 320, 60, 95); //øverste del af øret (højre)
  ellipse(1200, 345, 40, 65);//nederste del af øret (højre)
}
 // Ansigtet på cirkel 1
 fill(100);
 ellipse(350, 270, 55, 75);
 ellipse(450, 270, 55, 75);
 rect(285, 380, 230, 15);

 // Ansigtet på cirkel 2
 push();
 //øjnene 
  fill(240);
  ellipse(930, 270, 80, 65);
  ellipse(1090, 270, 80, 65);
  fill(110);
  ellipse(930, 270, 45, 40);
  ellipse(1090, 270, 45, 40);
  rect(970,420,80,10); // munden

  // Næsen 
  stroke(110);
  strokeWeight(23);
  point(1005,370); 
  point(1025,370);
  stroke(240);
  strokeWeight(10);
  point(1005,370);
  point(1025,370);
  pop();
 
 //Min tekst 
  fill(120);
  textSize(25);
  stroke(60);
  text("HVAD FOR DENNE EMOJI DIG TIL FØLE?",175,90);

  fill(120);
  textSize(25);
  stroke(60);
  text("BLIVER DU PROVOKERET OVER DENNE EMOJI?",740,90);


push();
if(mouseX>=840 && mouseX<=940 && mouseY>=600 && mouseY<=700){ // For beige firkant 
  noFill();
  stroke(255,20,20);
  strokeWeight(9);
  ellipse(925, 270, 150, 100);
  ellipse(1095, 270, 150, 100);
  line(1000, 270, 1020, 270); //næsestreg
  line(850, 270, 838, 265);
  line(1182, 265, 1170, 270);
  // nu kommer mine punkter til fregnerne 
  fill(255,102,51); 
  stroke(255,102,51);
  strokeWeight(10);
  point(1130,370);
  point(1110,350);
  point(1105,385);
  point(1085,370);

  point(950,370);
  point(930,350);
  point(920,385);
  point(900,370);
  pop();
}
push();
if(mouseX>=960 && mouseX<=1060 && mouseY>=600 && mouseY<=700){ // For sandfarvet firkant 
  fill(0,0,153);
  noStroke();
  ellipse(930, 270, 25, 20);
  ellipse(1090, 270, 25, 20);
  fill(255,153,153); //kindeblush 
  noStroke();
  ellipse(1120, 350, 70, 30);
  ellipse(910, 350, 70, 30);
  stroke(255,51,0);
  strokeWeight(16);
  point(815,369); 
  point(1205,367);
  stroke(20);
  strokeWeight(5);
  line(880,250,890,260);
  line(895,230,905,245);
  stroke(20);
  strokeWeight(5);
  line(880,250,890,260);
  line(895,230,905,245);
  line(1140,250,1130,260);
  line(1130,230,1120,245);
}
pop();

push();
if(mouseX>=1080 && mouseX<=1180 && mouseY>=600 && mouseY<=700){
  fill(0);
  noStroke();
  // de to første ellipser er de sorte øjne 
  ellipse(930, 270, 25, 20);
  ellipse(1090, 270, 25, 20);
  // ellipserne nedenfor er håret 
  ellipse(900, 200, 10, 10);
  ellipse(920, 190, 10, 10);
  ellipse(940, 200, 10, 10);
  ellipse(940, 170, 10, 10);
  ellipse(960, 190, 10, 10);
  ellipse(990, 200, 10, 10);
  ellipse(990, 170, 10, 10);
  ellipse(970, 160, 10, 10);
  ellipse(1030, 200, 10, 10);
  ellipse(1080, 200, 10, 10);
  ellipse(1100, 190, 10, 10);
  ellipse(1010, 190, 10, 10);
  ellipse(1060, 190, 10, 10);
  ellipse(1120, 200, 10, 10);
  ellipse(1030, 170, 10, 10);
  ellipse(1070, 170, 10, 10);
  ellipse(1050, 160, 10, 10);
  ellipse(1010, 160, 10, 10);

  ellipse(815, 370, 10, 10); //ørering

  stroke(110);
  strokeWeight(50);
  point(995,370); 
  point(1025,370);
  stroke(0);
  strokeWeight(30);
  point(995,370);
  point(1025,370);
}
pop();
}

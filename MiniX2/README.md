# MiniX2

[Klik herpå for at se min MiniX2](https://kathrine2407.gitlab.io/aestetisk-programmering/MiniX2/index.html)

![](Skærmbillede_2023-02-19_kl._21.40.44.png)

[Selve koden](https://gitlab.com/kathrine2407/aestetisk-programmering/-/blob/main/MiniX2/MiniX2sketch.js)

### Beskrivelse af programmet 
Vi fik stillet til opgave at udforske former og geometriske figurer med en overvejende politisk påvirkning. Mange omkring mig fandt hurtigt ud af hvordan deres MiniX2 skulle være, hvor jeg brugte længere tid på blot at finde på en emoji at lave. 
Jeg startede med at defineret min baggrund til createCanvas(1500, 1500).

Mit program indeholder to emojis hvor hver emoji kan skifte farve samt ansigtselementer ud fra valgmulighederne indrammet af firkanter, når musen placeres i felterne. 

Emoji 1:
Den første emoji indeholder et monotont ansigt med to øjne og en mund. Når musen pacere en af de tre farver lyserød, grøn eller blå skifter farven på emojien, hvor øjne og mund bibeholdes. Ovenfor emojien er der en tekst som lyder på ”HVAD FOR DENNE EMOJI DIG TIL AT FØLE?”. Nedenfor ses et uddrag af de figurer jeg har anvendt til emojien. 

 fill(100);
 
 ellipse(350, 270, 55, 75);
 
 ellipse(450, 270, 55, 75);
 
 rect(285, 380, 230, 15);


Emoji 2: 
Den anden emoji indeholder også et monotont ansigt dog med to tilføjelser nemlig næsen og ørene. Dertil sker der både ændringer i farve og emojiens ansigt i form af briller, øreringe, hår osv. Ved at musens placering rykker sig fra den ende firkant til den anden. 

For at lave ansigtsform, øjne, hår og øreringe på har jeg gjort brug af forskellige syntakser på at lave cirkler for netop at få genskab til de forskellige syntakser som kan anvendes. Med det sagt har jeg gjort brug af point(), circle() og ellipse().  

For at der sker ændringer i de to grund emojis har jeg gjort brug af if statements, som beskriver noget ift. hvor musen er placeret på x og y-aksen, jeg har altså bestemt at hvis musen er placeret inden for eksempel den blå firkant skal cirklen som er tegnet tidligere blive farven blå i stedet for grå. På samme måde har jeg gjort følgende med den firkanter og ændringer. Et eksempel på nogle af de if statements jeg har foretaget på emoji 2. 

![](BilledeMiniX2.png)

Idet jeg har gjort brug af ret mange geometriske figurer har jeg været nødsaget til at bruge push() og pop() som indskrænker et stykke kode så det ikke bliver eller påvirker andre koder længere i programmet. 

Udover de to emojis har skrevet to forskellige tekster over hver af dem.

### Formålet - Social og kulturel kontekst (Identitet, race osv.)

Jeg har tænkt meget over spørgsmålet vi fik stillet i timen om - hvad et ansigt indeholder og hvor detaljeret beskrivelsen af et ansigt egentlig skal være. Mit formål med mine emojis er at kunne skelne mellem for meget og for lidt. For hvis vi får for mange muligheder kan det medvirke til man begynder ekskludere samt kategorisere menneskene. Med min emoji 2 har jeg forsøgt at skabe provokation ved netop generalisere forskellige racer. Jeg for eksempel defineret hvordan en rødhåret ser ud, i form af briller, fregner og lys hudfarve. Hvis man netop tager denne emoji i kontrast til min emoji 1, vil det være de færreste som føle sig ekskluderet eller generaliseret idet ingen mennesker er født med en blå, grønne eller røde hudfarve. For hurtigt at afrunde min pointe vil det altså sige, at hvis et individ har for mange valgmuligheder, kan der skabes en vis form for provokation og undrende spørgsmål og dertil vil nogen også føle sig ekskluderet. Så selvom man har til formål at være inkluderende kan den modsatte effekt opstå. 

### Udfordringer med programmet 

Jeg har for første gang gjort brug af push() og pop() syntakten og jeg er ikke helt sikker på hvorfor den kun fungere nogle gange og andre gange ikke. 
Når jeg har foretaget mine if statements forstår jeg ikke helt hvorfor jeg ikke kan sætte alle mine funktioner ind i et statement fremfor at lave en helt ny. Måske det har noget at gøre med rækkefølgen?
En anden udfordring jeg stød på var når jeg gerne ville forsøge at lave min canvas windowWidth og windowHeight og få mine emojis til at være præcis på hver sin halvdel af siden. Jeg kunne godt for det til at fungere med emoji 1 ved at dividere med 4 men den anden kunne jeg ikke få til!

### Opgraderingsmuligheder 

En af mine opgraderingsmuligheder kunne være at der er man selv kunne designe sin egen personlige emoji ud fra hvad man selv føler kunne definere en. For eksempel kunne man vælge tykkelse, størrelse, farve og så videre på det valgte element. Det skal ikke være for tidskrævende for ellers ville effekten med en emoji miste den effektivisering den grundlæggende har for en samtale.  






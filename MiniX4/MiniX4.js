//Variabler for jakke reklamerne
let imgsandfrakke;
let imggrafrakke;
let imgsortfrakke;
let imgbeigejakke;
let imgbrunjakke;
let imgkortjakke;
let imglæderjakke;

//Variabler for bukse reklamerne
let imgjeans;
let imgsortebukser;
let imgsandbukser;
let imgblajeans;

//Variabler for sneakers
let imgregnbuesko;
let imgairforce;
let converse;
let sortairforce;

function setup() {
  // put setup code here
  createCanvas(1450, 750);

  //Alle jakker
  imgsandfrakke = loadImage('frakkesand.png');
  imggrafrakke = loadImage('grafrakke.png');
  imgsortfrakke = loadImage('sortfrakke.png');
  imgbeigejakke = loadImage('beigejakke.png');
  imgbrunjakke = loadImage('brunjakke.png');
  imgkortjakke = loadImage('kortjakke.png');
  imglæderjakke = loadImage('læderjakke.png');

  //Alle bukser 
  imgjeans = loadImage('jeans.png');
  imgsortebukser = loadImage('sortebukser.png');
  imgsandbukser = loadImage('sandbukser.png');
  imgblajeans = loadImage('blajeans.png');

  //Alle sneakers
  imgregnbuesko = loadImage('regnbuesko.png');
  imgairforce = loadImage('airforce.png');
  imgconverse = loadImage('converse.png');
  imgsortairforce = loadImage('sortairforce.png');
}

function draw() {
  background(255);

  //Teksten øverst
  fill(50);
  textSize(50);
  stroke(100);
  text("WEEKDAY",1450/4, 750/8);

  fill(100);
  textSize(18);
  noStroke(0);
  text("Women",1450/4, 750/6);
  text("Men",1450/4+73, 750/6);
  text("Jeans",1450/4+120, 750/6);

  fill(150);
  textSize(18);
  noStroke(0);
  text("Reklame",100,50);
  text("Reklame",1280,50);

  //Rammerne i lysegrå bag tøjet
  fill(230);
  noStroke(0);
  rectMode(CENTER);
  rect(475, 400, 240, 450);
  rect(725, 400, 240, 450);
  rect(976, 400, 240, 450);

 // De to lodrette streger
 fill(220);
 rect(1170, 375, 1, 750);
 rect(280, 375, 1, 750);

//Stregen derkommer til syne under "Women", "Men" og "Jeans" når musen passere ordene
  if(mouseX>=360 && mouseX<=436 && mouseY>=110 && mouseY<=135){ 
    fill(100);
    rect(394, 130, 62, 1);
  }
  if(mouseX>=437 && mouseX<=470 && mouseY>=110 && mouseY<=135){
    fill(100);
    rect(452, 130, 34, 1);
  }
  if(mouseX>=475 && mouseX<=530 && mouseY>=110 && mouseY<=135){
    fill(100);
    rect(507, 130, 46, 1);
  }
  
  // Hjertene til at like en vare
  noStroke(0);
  fill(100);
  text("♡",560,210);
  text("♡",810,210);
  text("♡",1060,210);

 //If statement for at få hjertet rødt
if(mouseX>=1065 && mouseX<=1080 && mouseY>=203 && mouseY<=220){ 
  fill(255,0,0);
  text("♥",1060,209.5);
}
if(mouseX>=805 && mouseX<=825 && mouseY>=203 && mouseY<=220){ 
  fill(255,0,0);
  text("♥",810,209.5);
}
if(mouseX>=555 && mouseX<=570 && mouseY>=200 && mouseY<=220){ 
  fill(255,0,0);
  text("♥",560,209.5);
}
  //De nederste bjælker
  //Den inderste 
  fill(230);
  noStroke(10);
  rectMode(CENTER);
  rect(475, 660, 240, 50);
  rect(725, 660, 240, 50);
  rect(976, 660, 240, 50);

  //Ovenpå liggende bjælke
  fill(220);
  rect(475, 660, 220, 30);
  rect(725, 660, 220, 30);
  rect(976, 660, 220, 30);
  
  //Tekst til bjælken 
  fill(100);
  textSize(15);
  noStroke(0);
  text("TILFØJ TIL KURV",370, 665);
  text("TILFØJ TIL KURV",620, 665);
  text("TILFØJ TIL KURV",870, 665);
  
//Frakke 
image(imgsandfrakke,380,260,190,280);

//Frakke reklamer   
if (mouseX>300 && mouseX<600 && mouseY>645 && mouseY<675){
if (mouseIsPressed){
image(imggrafrakke,70,80,130,200);
image(imgsortfrakke,70,300,140,210);
image(imgbeigejakke,80,530,120,190);
image(imgbrunjakke,1210,530,200,190);
image(imgkortjakke,1210,330,200,190);
image(imglæderjakke,1250,80,150,190);
}  
}
push()
//Jeans
image(imgjeans,580,280,300,260);

//Jeans reklamer 
if (mouseX>620 && mouseX<840 && mouseY>645 && mouseY<675){
if (mouseIsPressed){
image(imgblajeans,50,80,170,200);
image(imgsortebukser,30,300,200,210);
image(imgsandbukser,70,530,120,190);
image(imgsandbukser,1210,530,200,190);
image(imgblajeans,1210,300,200,190);
image(imgsortebukser,1250,80,150,190);
}
}
pop()
//Sneakers 
image(imgairforce,865,280,220,220);

//Sneakers reklamer 
if (mouseX>860 && mouseX<1085 && mouseY>645 && mouseY<675){
if (mouseIsPressed){
image(imgregnbuesko,70,80,150,160);
image(imgregnbuesko,1250,530,150,160);
image(imgconverse,90,320,120,120);
image(imgconverse,1260,330,120,120);
image(imgsortairforce,1230,130,180,120);
image(imgsortairforce,50,530,180,120);
}
}
}
 




# MiniX4

[Klik herpå for at se min MiniX4](https://kathrine2407.gitlab.io/aestetisk-programmering/MiniX4/index.html)

![](Skærmbillede_2023-03-13_kl._08.06.48.png)

[Selve koden](https://gitlab.com/kathrine2407/aestetisk-programmering/-/blob/main/MiniX4/MiniX4.js)


### Til festivalen (Transmediale)

Mit program hedder “Du får hvad du klikker”. De fleste af os kender det at man har været inde på en hjemmeside og surfede lidt rundt efter noget nyt til garderoben. Efter at man har været inde på forskellige hjemmesider i noget tid og klikkede på forskellige beklædningsdele, mindskes eller forøges lysten og produktet bliver enten købt eller siden bliver forladt. Men uden du ved af det, har computeren opfanget de forskellige produkter du har været inde på og der popper nu reklamer op på din facebook, instagram og andre hjemmesider som ikke har noget med den tidligere hjemmeside at gøre. Og netop dette understreger mit program. Alt data bliver opfanget, om man lyster eller ej. Så selvom den tidligere side skulle være lukket ned, fremstår den nu som en reklame- og datafangst. 



### Beskrivelse af programmet samt tilvalg og fravalg
Vi generere data hver dag og det er forbløffende hvor meget vi selv er med til at skabe vores egne reklamer og hjemmesider udfra hvad vi tidligere har været inde på. Vi alle oplevet at man har været inde på en bestemt tøj hjemmeside og efterfølgende hvis man går på facebook opstår præcis reklamer for det netop det tøj du har set på, for få minutter siden. Netop dette har jeg forsøgt at illustrere med min MiniX4. Programmet forstiller tøj hjemmesiden WEEKDAY. Der ses tre forskellige stykker beklædning. En frakke, et par bukser og et par sko. Hvis musen passere de små hjerter ved hvert beklædningsdel ændres den til rød, som i at du kan tilføje varen til din favoritliste. På samme måde er der tilføjet en detalje streg under "Women" "Men" "Jeans", idet musen er ved kategorierne. Hvis man klikker på "tilføj kurv" ved de forskellige vare opstår der reklamer for  den valgte tøjbekldning. Netop dette er min pointe med data capture. Hvis man ønsker et par bukser, vil du uden tvivl få reklamer lignende eller de eksakte bukser på andre platforme. 

Jeg har altid godt vist at data bliver indsamlet til at personificere outputtet til brugeren, men jeg har ikke haft kendskab til hvordan. Som der bliver beskrevet i teksten, gøres der for eksempel brug af noget der kaldes for heatmap: 

> _Heatmap is one of the visualization tools and provides a graphical representation of data to visualize user behavior. It is commonly used in industries for the purpose of data analytics. For example, it is easy to track the cursor’s position and compute the duration of its stay in different areas of a web page, providing an indication as to which content is “hotter”_

Jeg synes det vildt at tænke på at teknologien er så fremme at man kan observere brugerens handlinger uden at brugeren selv er klar over det. For eksempel hvor længe man befinder sig det samme sted på skærmen og hvor. Dertil også de forskellige funktioner der tages i brug for at opfange nok data til at fremstille noget brugervenligt output. Og ja ikke for at sige hvor personificerede outputtet egentlig kan blive. 

### Selve koden
I mit program har jeg gjort brug af forskellige if statements. Ved hjerterne og stregen under "Women" "Men" "Jeans". Nedenfor ses if statemenet for understregene.

![](Skærmbillede_2023-03-13_kl._08.50.15.png)

Som tidligere nævnt har jeg gjort brug af mange billeder og for at få et billedet ind på sin js fil, kræver det først at man downloader det valgte billede ned på sin computer og dernæst tilføje den. Måden man tilføjer et billede er ved først at definere en varibalet ud fra det valgte billede, dernæst skal tvinge billedet frem ved at bruge "loadImage" som siger at den skal finde den valgte fil frem fra computer arkivet. 
Når det så er gjort tilføjer man en x og y koordinat - som er bestemmelsen af placering af billedet på canvasset. Hvis man ønsker at ændre billedstørrelsen, tilføjer man yderligere en højde og bredde. Så grundlæggende lyder koden således: image(imgbillede,x,y,h,b);

let imgsandfrakke;

function setup() {
imgsandfrakke = loadImage('frakkesand.png');

function draw() {
image(imgsandfrakke,380,260,190,280);

Jeg har også anvendt tekst syntaks, både til mine ord men også hjerterne hvor jeg har gjort brug af hjerteemoji. Måden jeg har gjort det på er ved at skrive text("") og så tilføje den ønskede tekst. Dertil kan man vælge størrelse på teksten ved at skrive textSize("). Ydermere kan man vælge hvilken farve teksten skal have ved bruge syntaksen fill(").

### Forbedringer og refleksioner 
Eftersom jeg har efterlignet en hjemmeside, mangler der en del deltaljer ved mit program, da hjemmesiden WEEKDAY indeholder en del flere end min. Jeg vil gerne have ønsket at der ville kunne ske en ændring ved at skifte kategori eller måske helt platform. 
Idet jeg har gjort brug af rigtig mange billeder, ville jeg gerne vide om der er en smartere måde at samle netop alle disse billeder i en folder eller lign for at man kan oploade dem på en smarte måde. 

Nogle af mine reklame billeder kommer ikke frem selvom jeg har tilføjet dem og hvorfor det ved jeg ikke helt lige. Dette er helt klart en forbedring jeg vil ligge fokus på. 

### Reference 

https://www.weekday.com/en_dkk/search.html?q=coat
https://bengrosser.com/projects/facebook-demetricator/?fbclid=IwAR0IQFl7Bsqst9MY28AVnYmnY-rptlTy8cWknJMk9VfTyThaPmOl4gCC1D8




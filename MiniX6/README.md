# MiniX6


[Start æblefangst](https://kathrine2407.gitlab.io/aestetisk-programmering/MiniX6/index.html)


![](Skærmbillede_2023-03-27_kl._21.17.13.png)


[Min kode til spil](https://gitlab.com/kathrine2407/aestetisk-programmering/-/blob/main/MiniX6/frugtspil.js)


[Class for kurv](https://gitlab.com/kathrine2407/aestetisk-programmering/-/blob/main/MiniX6/Kurv.js)


[Class for æbler](https://gitlab.com/kathrine2407/aestetisk-programmering/-/blob/main/MiniX6/Æbler.js)


### Beskrivelse af spillet og reflektioner
Spillet foregår i en skov fyldt med æbletræer. Idet æbler rådner eller er færdig modne, falder de ofte af træets gren og ned på jorden, for enten at skulle gå i en yderligere forrådnelse eller blive brugt til madlavning. I mit barndomshjem har vi to store æbletræer og det værste jeg vidste var at træde i et råddent æble eller faktisk også et frisk æble (av). Derfor har jeg indsat en kurv nederst på canvaset, til at fange alle de faldne æbler. Og hermed kan der gå konkurrence i den - fang så mange æbler som muligt. 

Spil er noget der virkelig har præget min barndom og især digitaliserede spil, hvor man enten skal bruge piletasterne eller et joystick - så det at sidde og programmere et spil selv, finder jeg meget interessant og også ret spændende. Jeg har aldrig rigtig tænkt over hvordan spil egentlig er lavet, jeg har blot benyttet mig af dem. Nu hvor jeg selv har siddet og skulle finde på mit eget spil har skabt en del udfordringer, eftersom jeg altid tror jeg skal opfinde den dybe tallerken, som jeg jo lang fra kan, med mine nuværende programmerings evner. Da det er lang tid siden jeg selv har spillet et digitalt spil har jeg ikke haft et specifik formål med denne minix, sammenlignet med de tidligere minix'er. 
Jeg har egentlig “bare” gjort brug af den skabelon som blev brugt til undervisningen. Min idefase for denne omgang har været meget tom og jeg har ikke rigtig “gidet” at lægge tid og kræfter i den - hvorfor, ved jeg egentlig ikke helt…

Imens jeg har skrevet min Readme er det gået op for mig - uden at tænke over - at mit spil afspejler den virkelige verden. Som skrevet i citatet nedenfor, har spillet givet mig nogle refleksioner over hvordan jeg måske selv kan optimere min måde at samle æbler op på, det med at gøre det sjovt “en form for spil” kontra en pligt. 

“It’s also worth reiterating that OOP is designed to reflect Af David Reinfurt the way the world is organized and imagined, at least from the computer programmers’ perspective” (Soon & Cox, 2020, p. 160) 

Det at man automatisk trækker afspejler den virkelig verden, synes jeg er ret tankevækkende og interessant. 

### Forklaring af koderne

Jeg har 3 js fil - den ene som indeholder de funktioner der skal forekomme på mit canvas ud fra de to classes jeg har lavet til kurven og de faldne æbler. 

I min class kurven (Player) har objektet egenskaberne at den skal være et billede af kurven png ud fra nogle x og y variabler. Dertil har kurven methods hvor den kan bevæge sig fra højre til venstre med brugen af piletasterne. 

Den anden class æbler indeholder på samme måde nogle variabler og en af de interessante variabler er this.speed som er lavet ud fra random skala 0.2 til 2, som anvendes min method move, hvor de skal komme på skærmen inden for random spektret. 

Ved at have dannet de to classes kan jeg kalde på deres metoder i min “hoved fil”, her har jeg gjort brug af for loop og arrays for at generere flere æbler ad gangen end kun den ene som er i classen. Dertil har jeg også gjort brug af if statement til min point. Som siger at hvis distancen fra et æble og kurven er halvdelen af kurvens størrelse, skal den “fange” æblet og der skal komme et nyt og point tæller stiger med 1. 

if (distance < player.size / 2) {

tomato.splice(i, 1);

tomato.push(new Tomato());

point++ //betyder at man lægger 1 til variablen point  }

For at forbedre mit spil kunne man gøre sådan at det var et endegyldigt mål, så det ikke bare fortsætter ud i det uendelige, samt tilføje en funktion hvor man kan tabe. For netop at gøre spillet sjovere og mere konkurrence agtig. 

#### Yderligere tanker 

Jeg har altid været vild med den tidsfordriv og begejstring man får over at vinde baner og eller stige i niveauer i et spil, men hvornår jeg stoppede med at finde det sjovt? Hvornår gik jeg væk fra den digitale spil verden til et helt almindelig slag kort og eller terningspil? Er det fordi jeg er blevet ældre og man føler sig mere nærværende med menneskene omkring sig når man spiller ansigt til ansigt og ikke over en skærm? 



let tomato = [];
let player;
let point = 0; //pointtæller starter fra 0

let antal = 10; //antallet af æbler på skærmen hele tiden 

//Billeder bliver defineret så de kan anvendes
function preload() {
    kurven = loadImage("kurv.png");
    æble = loadImage("æble.png");
    baggrund = loadImage('skov.png');
}

function setup() {
    createCanvas(windowWidth, windowHeight);

    //For loop som siger at der skal komme nye æbler på skærmen ud fra det givne antal som er defineret til 10
    for (let i = 0; i < antal; i++) {
        tomato.push(new Tomato());
    }

    player = new Player();
}

function draw() {
    background(0, 250, 0);
    image(baggrund, 20, 0, windowWidth - 45, windowHeight);
    spawnTomato();
    player.show();
    checkCollision();

    //Teksten øverst på min Canvas 
    textAlign(CENTER);
    textSize(30);
    textFont('Futura'); //Skrifttypen 
    strokeWeight(2);
    stroke(0, 102, 0);
    fill(255, 204, 0);
    text("FANG ALLE ÆBLER MED KURVEN", width / 2, 50);
    noStroke(0, 102, 0);
    fill(0, 223, 51);
    text("Point: " + point, width / 2, 90);
}

// Besrkivelse af 
function spawnTomato() {
    for (let i = 0; i < tomato.length; i++) {
        tomato[i].move();
        tomato[i].show();
    }
}
//Når piltasterne til højre og venstre er trykket på 
function keyPressed() {

    if (keyCode === LEFT_ARROW) {
        player.moveLeft();
    }
    if (keyCode === RIGHT_ARROW) {
        player.moveRight();
    }
    if (keyCode === ENTER) {
        setup();
        spawnTomato();
        player.show();
        checkCollision();
    }
}

function checkCollision() {
    let distance;
    for (let i = 0; i < tomato.length; i++) {
        distance = dist(player.posX + player.size / 2, player.posY + player.size / 2, tomato[i].posX, tomato[i].posY);

        if (distance < player.size / 2) {
            tomato.splice(i, 1);
            tomato.push(new Tomato());
            point++ //betyder at man lægger 1 til variablen point 

        }
    }
}

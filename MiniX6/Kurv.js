class Player {
    constructor(){
        this.posX = width/2;
        this.posY = height - 50;
        this.speed = 50;
        this.size = 120;
    }

    moveLeft(){
        this.posX -= this.speed;
    }
    moveRight(){
        this.posX += this.speed;
    }

    show(){
        image(kurven, this.posX, this.posY, this.size,50);
    }
}



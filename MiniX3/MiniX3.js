// Variabler for bogstavet L
let cirx =50; 
let ciry=250;
let cirs=30;
let speed1=1;

//Varibaler for bogstavet O
let rectx=200;
let recty=230;
let rectb=20;
let recth=35;
let angle=0;

//Variabler for bogstavet A
/* Jeg har gjort brug af varibalerne for 
    bogstavet L så figurene bevæger sig 
    på samme måde
*/

//Variabler for bogstavet D
let ellix=570;
let elliy=240;
let ellib=35;
let ellih=40;

//Variabler for bogstavet I
// x-værdien for I er variablen cirx
let squary=200;
let squars=35;

//Variabler for bogstavet N
let cirnx=950;
let cirny=230;
let cirns=40;

//Variabler for bogstavet G
/* Jeg har gjort brug af varibalerne for 
    bogstavet L så figurene bevæger sig 
    på samme måde
*/

function setup() {
//create a drawing canvas
createCanvas(1500, 700);
angleMode(DEGREES);

}
function draw() {
background(153,204,153);

//Bogstavet L
//Højden 
noStroke();
fill(25);
circle(cirx,ciry,cirs);
circle(cirx,ciry+50,cirs);
circle(cirx,ciry+100,cirs);
circle(cirx,ciry+150,cirs);
circle(cirx,ciry+200,cirs);

//Bredden
circle(cirx+50,ciry+200,cirs);
circle(cirx+100,ciry+200,cirs);

cirx = cirx + speed1;

if (cirx <= 40){
    speed1 = speed1 * -1
} else if (cirx >= 60){
    speed1 = speed1 * -1
}

//Bogstavet O
push();
translate(rectx+5,recty+22.5);
rotate(angle);
rectMode(CENTER);
rect(0, 0, rectb, recth);
angle = angle +0;
pop();

push();
translate(rectx+5,recty+63+22.5);
rotate(angle);
rectMode(CENTER);
rect(0, 0, rectb, recth);
angle = angle +0;
pop();

push();
translate(rectx+5,recty+127+22.5);
rotate(angle);
rectMode(CENTER);
rect(0, 0, rectb, recth);
angle = angle +0;
pop();

push();
translate(rectx+5,recty+186+22.5);
rotate(angle);
rectMode(CENTER);
rect(0, 0, rectb, recth);
angle = angle +0;
pop();

push();
translate(rectx+5+125,recty+22.5);
rotate(angle);
rectMode(CENTER);
rect(0, 0, rectb, recth);
angle = angle +0;
pop();

push();
translate(rectx+5+125,recty+63+22.5);
rotate(angle);
rectMode(CENTER);
rect(0, 0, rectb, recth);
angle = angle +0;
pop();

push();
translate(rectx+5+125,recty+127+22.5);
rotate(angle);
rectMode(CENTER);
rect(0, 0, rectb, recth);
angle = angle +0;
pop();

push();
translate(rectx+5+125,recty+186+22.5);
rotate(angle);
rectMode(CENTER);
rect(0, 0, rectb, recth);
angle = angle +0;
pop();

push();
translate(rectx+22.5+20,recty+5);
rotate(angle);
rectMode(CENTER);
rect(0, 0, rectb, recth);
angle = angle +0;
pop();

push();
translate(rectx+22.5+20,recty+5);
rotate(angle);
rectMode(CENTER);
rect(0, 0, rectb, recth);
angle = angle +0;
pop();

push();
translate(rectx+22.5+70,recty+5);
rotate(angle);
rectMode(CENTER);
rect(0, 0, rectb, recth);
angle = angle +0;
pop();

push();
translate(rectx+22.5+20,recty+220+5);
rotate(angle);
rectMode(CENTER);
rect(0, 0, rectb, recth);
angle = angle +0;
pop();

push();
translate(rectx+22.5+70,recty+220+5);
rotate(angle);
rectMode(CENTER);
rect(0, 0, rectb, recth);
angle = angle +5;
pop();

//Bogstavet A
square(cirx+380,ciry-20,cirs,5);
square(cirx+380,ciry+100,cirs,5);

//venstre højde
square(cirx+430,ciry+20,cirs,5);
square(cirx+430,ciry+70,cirs,5);
square(cirx+430,ciry+130,cirs,5);
square(cirx+430,ciry+190,cirs,5);

//højre højde
square(cirx+330,ciry+20,cirs,5);
square(cirx+330,ciry+70,cirs,5);
square(cirx+330,ciry+130,cirs,5);
square(cirx+330,ciry+190,cirs,5);

//Bogstavet D
//venstre højde
push();
translate(ellix,elliy);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, ellib, ellih);
angle = angle +0;
pop();

push();
translate(ellix,elliy+70);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, ellib, ellih);
angle = angle +0;
pop();

push();
translate(ellix,elliy+140);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, ellib, ellih);
angle = angle +0;
pop();

push();
translate(ellix,elliy+210);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, ellib, ellih);
angle = angle +0;
pop();

//højre højde
push();
translate(ellix+140,elliy+70);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, ellib, ellih);
angle = angle +0;
pop();

push();
translate(ellix+140,elliy+140);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, ellib, ellih);
angle = angle +0;
pop();

//Bue
push();
translate(ellix+100,elliy+10);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, ellib, ellih);
angle = angle +0;
pop();

push();
translate(ellix+100,elliy+200);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, ellib, ellih);
angle = angle +0;
pop();

//Vandret ellipser
push();
translate(ellix+40,elliy-20);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, ellib, ellih);
angle = angle +0;
pop();

push();
translate(ellix+40,elliy+230);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, ellib, ellih);
angle = angle +0;
pop();

//Bogstavet I
//bredden

rect(cirx+715, squary+10, 30,30); 
rect(cirx+752, squary+10, 30,30); 
rect(cirx+789, squary+10, 30,30); 

rect(cirx+715, squary+250, 30,30);
rect(cirx+752, squary+250, 30,30);
rect(cirx+789, squary+250, 30,30);

//højden
rect(cirx+755, 255, squars-10,20);
rect(cirx+755, 295, squars-10,20);
rect(cirx+755, 335, squars-10,20);
rect(cirx+755, 375, squars-10,20);
rect(cirx+755, 415, squars-10,20);

//Bogstavet N
//venstre højde
push();
translate(cirnx,cirny);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, cirns, 50);
angle = angle +0;
pop();

push();
translate(cirnx,cirny+120);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, cirns, 50);
angle = angle +0;
pop();

push();
translate(cirnx,cirny+235);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, cirns, 50);
angle = angle +0;
pop();

//højre højde
push();
translate(cirnx+180,cirny);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, cirns, 50);
angle = angle +0;
pop();

push();
translate(cirnx+180,cirny+120);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, cirns, 50);
angle = angle +0;
pop();

push();
translate(cirnx+180,cirny+235);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, cirns, 50);
angle = angle +0;
pop();

//diagonalen
push();
translate(cirnx+30,cirny+60);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, cirns, 20);
angle = angle +0;
pop();

push();
translate(cirnx+90,cirny+120);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, cirns, 20);
angle = angle +0;
pop();

push();
translate(cirnx+150,cirny+180);
rotate(angle);
ellipseMode(CENTER);
ellipse(0, 0, cirns, 20);
angle = angle +0;
pop();


//Bogstavet G
//højden
square(cirx+1150,ciry-30,cirs,5);
square(cirx+1150,ciry+20,cirs,5);
square(cirx+1150,ciry+70,cirs,5);
square(cirx+1150,ciry+120,cirs,5);
square(cirx+1150,ciry+170,cirs,5);
square(cirx+1150,ciry+220,cirs,5);

//vandrette
//øverst
square(cirx+1200,ciry-30,cirs,5);
square(cirx+1250,ciry-30,cirs,5);
square(cirx+1300,ciry-30,cirs,5);
//nederst
square(cirx+1200,ciry+220,cirs,5);
square(cirx+1250,ciry+220,cirs,5);
square(cirx+1300,ciry+220,cirs,5);
//det der viser det er et G
square(cirx+1300,ciry+120,cirs,5);
square(cirx+1300,ciry+170,cirs,5);
square(cirx+1250,ciry+120,cirs,5);

}

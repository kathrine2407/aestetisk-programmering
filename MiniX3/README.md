# MiniX3

[Klik herpå for at se min MiniX3](https://kathrine2407.gitlab.io/aestetisk-programmering/MiniX3/index.html)

![](Skærmbillede_2023-03-05_kl._20.50.19.png)

[Selve koden](https://gitlab.com/kathrine2407/aestetisk-programmering/-/blob/main/MiniX3/MiniX3.js)

### Beskrivelse af programmet samt tilvalg og fravalg

Min throbber danner ordet LOADING. Hvert bogstav er dannet ud fra forskellige syn takster og geometriske figurer. Her er der blevet gjort brug af ellipser, square, circles osv. Min proces til at komme til netop denne throbber tog rimelig lang tid, jeg var meget i tvivl om hvad jeg ville lave og dagene gik. Så jeg besluttede mig for at selve throbberen ikke skulle være noget specielt pænt eller fancy, men mit fokus skulle ligge på at jeg skulle have styr på flere forskellige syntakser jeg har haft svære ved at forstå. Som for eksempel translate og rotate som jeg har anvendt ved hvert andet bogstav.
Udover at få bedre kendskab til forskellige syntakser, skulle min throbber indeholde en vis form for bevægelse som skulle være med til at skabe den tidsrelation som oftes forbindes med en throbber/loading. En throbber er nemlig til for at kommunikere at computeren arbejder (tænker) og der foregår operationer bag throbberen som det almene mennesker ikke har forståelse for. Throbberen kommunikere altså til brugeren at computeren er i gang med nogle processer for at kunne gennemføre en handling.
Vi som mennesker kan hurtigt blive utålmodige hvis vores teknologier ikke virker og derfor er en throbber væsentlig. Formålet med at der er bevægelse skyldes at vi får en fornemmelse af at der sker noget i nuet, netop realtiden. Computeren arbejder og handlinger foretages, hvor modsat hvis skærmen blot blev hvid eller sort, frister det os til at konkludere at programmet ikke virker, og endvidere slukkes eller lukkes ned for. 

### Selve koden

Før jeg fik mine bogstaver til at bevæge sig har jeg først lavet bogstaverne ud fra en masse variabler. Efter jeg har fået dem til at være som ønsket har jeg skabt bevægelse. Måden jeg har fået mine bogstaver L, A, I og G til at bevæge sig i vandret retning ergo x-aksen, er ved en en if statement som ses nedenfor: 

![](If_statement.png)

Den fortæller at hver gang varibalen cirx, som er 50, er mindre eller lig med 40 skal speed bevæge sig i - x aksen og når cirx er større eller lig med 60 skal den bevæge sig den modsatte vej. Så cirklen skal altså bevæge sig mellem x koordinaterne 40 → 60, hvor hastigheden af bevægelsen kan ændres ud fra variablen speed. Idet jeg ønskede at hver andet bogstav skulle bevæge sig på samme måde, har jeg brugt variablen cirx på alle de gældende bogstaver. 

Derudover ønskede jeg at gøre brug af translate og rotate funktionen ved de andre bogstaver. Figurene som dannede bogstaverne rotere om sit eget midtpunkt og dette gøres ved at definere origo til figurens midte samt fortælle at den anvendte figurer som for eksempel rect skal være i centrum:  rectMode(CENTER)

![](Bogstavet_O.png)

For at alle de mange figurere rotationerne ikke skulle påvirke hinanden i koden har jeg gjort brug af push() og pop(). 

### Forbedringer og refleksioner 

Som tidligere nævnt havde jeg til formål at bruge så mange syntakser som “muligt” men eftersom jeg ikke kunne få nogle af dem til at fungere har jeg kun gjort brug af variabler, if statement, translate og rotate. Jeg vil gerne have haft alle bogstaver til at bevæge sig forskelligt, både ud af y-aksen og gøre brug af loop. Derudover kun jeg godt have tænkt mig at jeg havde gjort brug af windowWidth og WindowHeight for netop at LOADING skulle være placeret i midten uafhængigt af browser. 

Når jeg kigger på min endelig throbber kan jeg sagtens se hvad der står og betydningen med den. Dog hvis jeg har spurgt andre om at se den har de svært ved at se betydningen og helt grundlæggende ordet, da nogle af bogstaverne minder om hinanden og nogle af bogstaverne ikke lever op til forventningerne. Dette kan skyldes at der sker bevægelse der måske er med til at forstyrre forståelsen mere end den gavner, samt at ordene ikke er tydeliggjorte nok. Hvis jeg skulle lave en helt nu throbber ville jeg nok lave en hvor man selv kan interagere så “ventetiden”, så den følelses kortere og det bliver en sjov oplevelse frem for en irritation som det ofte ender med at blive. 


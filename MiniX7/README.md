# MiniX7

[Se min forbedret havbund](https://kathrine2407.gitlab.io/aestetisk-programmering/MiniX7/index.html)

![](havbund.png)


[Havbund](https://gitlab.com/kathrine2407/aestetisk-programmering/-/blob/main/MiniX7/havbund.js)


[Class for Blåfisk](https://gitlab.com/kathrine2407/aestetisk-programmering/-/blob/main/MiniX7/Blafisk.js)


[Class for Rødfisk](https://gitlab.com/kathrine2407/aestetisk-programmering/-/blob/main/MiniX7/Rodfisk.js)

#### Hvilken miniX har jeg valgt?

Til at starte på havde jeg svært ved at finde ud af hvilken minix jeg ville genbesøge, da de fleste kunne være interessante at ændre på og tilføje nye syntakser. Jeg kiggede mine forskellige minixér igennem og fandt frem til at det kunne være spændende at genbesøge min første minix, idet man kan se den udvikling af syntakser jeg har tillært gennem kurset men også fordi jeg til at starte med har hardcoded en del og det derfor kunne være fedt at få min “havbund” skrevet på en kortere og nemmere måde end jeg har gjort i minix1. Mit genbesøg er altså til min miniX1. 

#### Hvilke ændringer har jeg lavet og hvorfor?

Et af mine formål med mit genbesøg var at få skrevet en kode som fyldte mindre og til dels var nemmere at læse, samt overskuelig. Jeg har derfor anvendt billeder frem for at hard code en fisk selv, da jeg ønskede at de “nye” fisk skulle have flere detaljer. Udover at benytte billeder til mine fisk, har jeg også gjort brug af billeder til mit tang, så alle mine objekter i programmet er reelt set billeder. 
Da jeg har anvendt mere end en fisk, har jeg valgt at gøre brug af objekt abstraction, hvor jeg har lavet to js filer, ergo en js fil for hver fisk, og givet dem forskellige elementer og funktioner. Dette har jeg gjort ved brug af ´class´. Hvor i mine constructoren har jeg givet dem random variabler ift den hastighed og placering fiskene har. 

I min havbund.js fil har jeg også anvendt variabler som bruges i mine forloop, som siger noget om hvor mange fisk der skal komme af fisk1 og fisk2, når de forsvinder fra canvasset. Ydermere har jeg gjort brug af arrays inde i mine forloops som beskriver at fiskene (billederne) skal bevæge sig i x-aksens retning, som stammer fra mine if statements i mine classes for de to fisk. 
Med brugen af mine forskellige js filer har jeg også forsøgt at gøre mit program mere læsevenligt, så andre udover mig selv kan forholde sig til det kode der er blevet skrevet, og man hurtigt kan skabe sig et overblik. 

Nedenfor ses ændring fra minix1 til minix7:
![](samligning.png)

#### Hvad har jeg lært i denne miniX? Hvordan kunne du inkorporere eller videreudvikle koncepterne i min ReadMe/RunMe baseret på litteraturen vi har haft indtil nu?

Med denne miniX har jeg fået skabt et lille overblik over hvor mange syntakser jeg egentlig har fået kendskab til. Hvilket er langt flere end jeg havde regnet med, da vi startede kurset. Ved uarbejdelsen af de mange forskellige minix’er i løbet af kurset, har jeg fået en forståelse for hvor mange elementer der kan indgå i forskellige programmer, men også hvor ukompliceret kodning også kan være, med de rigtige briller og forståelse. Før kurset har jeg ikke reflekteret eller stillet spørgsmålstegn omkring alle de digitale enheder jeg anvender dagligt. Jeg har fået øjnene op for, hvor meget et program kan indeholde - med forskellige hensigter og formål - samt hvilken rolle de forskellige enheder har og nogle mere end andre. Specielt emnet om emojien har fyldt meget for mig. Den refleksion omkring til- og fravalg, finder jeg meget interessant. Det med at, for mange muligheder kan skabe mere debat frem for modsat og det at en generativ kode kan skabe kunst er meget fængende. Men for at drage en parallel til min havbund, bliver rigtig meget programmeret ud fra det virkelige liv, hvilket min havbund tydeligt illustrerer. 

#### Hvad er relationen mellem æstetisk programmering og digital kultur? Hvordan demonstrere dit værk perspektiver i æstetisk programmeing (henvend evt. til forordet i grundbogen)?

Hvis jeg selv skulle beskrive ordet æstetisk programmering før kurset, handlede det for mig om det smukke i elementer og objekter, men efter at have haft dette kursus indtil videre indebærer det meget mere end som så. Som der bliver skrevet i teksten “preface” "the importance of programming — reading, writing and thinking with software — as a critical tool for our times, in recognition of the way in which our experiences are ever more programmed". Det handler ikke bare om at kunne programmere noget, der er rart for øjet, men noget som kan sættes i perspektiv og anses som kritisk design. Programmeringen skal have et formål og skal altså opnå noget med outputtet, man skal kunne læse, skrive og tænke gennem koden. 

Samfundet er i konstant forandring og i takt med dette ændres vores digitale kultur også. Meget af det, der bliver programmeret og fremstillet i dag drager paralleller til samfundet og det er derfor essentielt at kunne forholde sig kritisk og reflektere over de til- og fravalg et design kan tilbyde. At stille spørgsmålstegn til den eksisterende teknologi, men også det fremtidige er relevant. 




class Bla {
    constructor() {
        this.posX = random(0, width);
        this.posY = random(0, height - 280);
        this.speed = random(0.2, 2);
    }

    move() {
        this.posX += this.speed;
        if (this.posX > width) {
            this.posX = 0;
        }
    }

    show() {
        image(fisk, this.posX, this.posY, 200, 90);
        image(tang, -40, height-370, 600, 400);
        image(tang2, width-550, height-400, 600, 400);

      
    }
}


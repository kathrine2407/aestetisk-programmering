//defineret variabler som anvendes til forloop
let bla = [];
let rod = [];
let antal1 = 3;
let antal2 = 4;

function setup() {
    createCanvas(windowWidth, windowHeight);
    //forloop til de blå fisk
    for (let i = 0; i < antal1; i++) {
        bla.push(new Bla());
    }
    //forloop til de røde fisk
    for (let i = 0; i < antal2; i++) {
        rod.push(new Rod());
    }
}
 
function draw() {
    //baggrundsfarverne og den sandfarvet bund 
    background(51, 102, 200);
    fill(250, 204, 102)
    noStroke();
    square(1, 650, width);
    spawnBla();
    spawnRod();
}

function spawnBla() {
    for (let i = 0; i < bla.length; i++) {
        bla[i].move();
        bla[i].show();
    }
}

function spawnRod() {
    for (let i = 0; i < rod.length; i++) {
        rod[i].move();
        rod[i].show();
    }
}

//billeder hentet fra nettet bliver defineret og kaldt på her 
function preload() {
    fisk = loadImage("fisk.png");
    fisk2 = loadImage("fisk2.png");
    tang = loadImage("koral.png");
    tang2 = loadImage("tang.png");
}

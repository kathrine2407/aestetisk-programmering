class Rod {
    constructor() {
        this.posX = random(0, width);
        this.posY = random(0, height - 400);
        this.speed = random(0.5, 3);
    }

    move() {
        this.posX += this.speed;
        if (this.posX > width) {
            this.posX = 0;
        }
    }

    show() {
        image(fisk2, this.posX, this.posY-100, 200, 90);
    }
}

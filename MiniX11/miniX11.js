//Globale variabler 
let start = false;
let mappe1;
let internet;
let notiknap;
let notiknap2;
let skriv;
let skrivbruger;
let capture;
//Her sættes instastart til false, som er en boolean output, som enten kan være true eller false
let instaStart = false;

//her preloader vi alle billeder og giver dem navne så de kan kaldes på
function preload() {
  bin = loadImage("billeder/bin.png");
  firefox = loadImage("billeder/firefox.png");
  indstillinger = loadImage("billeder/indstillinger.png");
  bitcoins = loadImage("billeder/bitcoins.png");
  internet = loadImage("billeder/internet.png");
  kalender = loadImage("billeder/kalender.png");
  kalenderplan = loadImage("billeder/kalenderplan.png");
  kryds = loadImage("billeder/kryds.png");
  mail = loadImage("billeder/mail.png");
  mappe = loadImage("billeder/mappe.png");
  wifi = loadImage("billeder/wifi.png");
  profil = loadImage("billeder/profil.png");
  startskaerm = loadImage("billeder/startskaerm.jpg");
  loginskaerm = loadImage("billeder/loginskaerm.jpeg");
  billeder = loadImage("billeder/billeder.png");
  udraab = loadImage("billeder/udraab.gif");
  chrome = loadImage("billeder/chrome.png");
  postnord = loadImage("billeder/postnord.png");
  instagram = loadImage("billeder/insta.jpeg");
  instagramKryds = loadImage("billeder/krydschrome.png");
  offentlig1 = loadImage("billeder/offentlig1.png");
  offentlig2 = loadImage("billeder/offentlig2.png");
  privatwifi = loadImage("billeder/privatwifi.png");
  hackedbillede = loadImage("billeder/hacked.gif");
  ingenInternetInsta = loadImage("billeder/ingeninternet.png");
}

//Kører en gang, når programmet startes
function setup() {
  createCanvas(windowWidth, windowHeight);
  //frameraten er ændret til 30 frames i sekundet i stedet for 60
  frameRate(30);
  //LOGINSKÆRMEN
  //baggrunddsbilledet
  image(loginskaerm, 0, 0, windowWidth, windowHeight);

  // push og pop isolerer billedet fra loginskærmen så alle billeder ikke får CENTERMODE
  push();
  //Billede af profilen på loginsiden
  imageMode(CENTER); //Gør sådan at billedets punkt er i midten istedet for venstre hjørne
  image(profil, width / 2, height / 2.5, 200, 200);
  pop();

  //Login knap på loginsiden, DOM ELEMENT (går ind og trumfer andre syntakser i sketch) 
  knap = createButton("LOGIN");
  knap.position(width / 2 - 100, height / 2 + 180);
  knap.size(200, 20);
  knap.style("background-color", "#CCD1D1"); //hex-farvekode der hører til HTML
  knap.style("color", "#000000");
  knap.mousePressed(loginSkærm); //Hvis knappen bliver trykket ned, så kalder den på funktionen loginSkærm
  //vi bruger mousePressed() til at specificere knappens funktion - hvad der sker når man trykker på den.

  //Alle knapperne nedenfor er DOM elementer 
  //Skriv brugernavn 
  fill(0);
  textSize(15);
  text("Brugernavn", width / 2 - 120, height / 2 + 50);
  skriv = createInput(" ");
  skriv.size(240);
  skriv.position(width / 2 - 120, height / 2 + 60);

  //Skriv adgangskode 
  fill(0);
  textSize(15);
  text("Adgangskode", width / 2 - 120, height / 2 + 100);
  skriv2 = createInput(" ");
  skriv2.size(240)
  skriv2.position(width / 2 - 120, height / 2 + 110);

  //STARTSKÆRMEN/HJEMMESKÆRMEN
  //billedmappen
  billedmappen = createImg("billeder/billeder.png");
  billedmappen.position(350, 150);
  billedmappen.style("width", "800px");
  billedmappen.hide(); //Kanppernes bliver gemt (hide) fordi ellers vises de allerede på startskærmen, senere kaldes der på dem 

  krydsknapbilleder = createButton("X");
  krydsknapbilleder.position(350, 150);
  krydsknapbilleder.size(40, 30);
  krydsknapbilleder.style("background-color", "#C8C8C8");
  krydsknapbilleder.style("color", "#000000");
  //Hvis knappen bliver trykket ned, så kalder den på funktionen skift
  krydsknapbilleder.mousePressed(skift);
  krydsknapbilleder.hide();

  billedmappeknap = createImg("billeder/mappe.png");
  billedmappeknap.position(200, 48);
  billedmappeknap.style("width", "60px");
  billedmappeknap.mousePressed(visbilleder);
  billedmappeknap.hide();

  internetknap = createImg("billeder/internet.png");
  internetknap.position(50, 50);
  internetknap.style("width", "60px");
  internetknap.hide();

  //BITCOIN SCAM BESKED
  bitcoinbesked = createImg("billeder/bitcoins.png");
  bitcoinbesked.position(width / 1.4768, height / 1.5221);
  bitcoinbesked.style("width", "450px");
  bitcoinbesked.style("height", "200px");
  bitcoinbesked.mousePressed(Hacked);
  bitcoinbesked.hide();
  //Sætter Tid, sådan at funktionen visBitcoins vil blive vist efter 35000 millisekunder = 35 sek (javaskript funktion)
  setTimeout(visBitcoins, 35000);

  krydsknapbitcoin = createButton("X");
  krydsknapbitcoin.position(width / 1.03821, height / 1.52219);
  krydsknapbitcoin.size(40, 30);
  krydsknapbitcoin.style("background-color", "#FFFFFF");
  krydsknapbitcoin.style("color", "#000000");
  krydsknapbitcoin.mousePressed(skift6);
  krydsknapbitcoin.hide();

  //MAIL 
  mailknap1 = createImg("billeder/mail.png");
  mailknap1.position(200, 300);
  mailknap1.style("width", "60px");
  mailknap1.mousePressed(visMail);
  mailknap1.hide();

  mailknap2 = createImg("billeder/mail.png");
  mailknap2.position(100, height / 1.0588);
  mailknap2.style("width", "40px");
  mailknap2.mousePressed(visMail);
  mailknap2.hide();

  notiknap = createImg("billeder/notifikation.png");
  notiknap.position(width / 11.52, height / 1.05882);
  notiknap.style("width", "20px");
  notiknap.hide();

  notiknap2 = createImg("billeder/notifikation.png");
  notiknap2.position(width / 5.87755102, height / 2.27129337);
  notiknap2.style("width", "20px");
  notiknap2.hide();
  //Sætter Tid, sådan at funktionen notifikationMail vil blive vist efter 27000 millisekunder = 27 sek (javaskript funkktion)
  setTimeout(notifikationMail, 27000);

  mailbilled = createImg("billeder/postnord.png");
  mailbilled.position(600, 200);
  mailbilled.style("width", "700px");
  mailbilled.hide();

  scamMail = createButton("TRYK HER FOR AT BETALE");
  scamMail.position(800, 500);
  scamMail.size(200, 20);
  scamMail.style("background-color", "#9999FF");
  scamMail.mouseReleased(Hacked);
  scamMail.hide();
  
  krydsknapmail = createButton("X");
  krydsknapmail.position(600, 200);
  krydsknapmail.size(40, 30);
  krydsknapmail.style("background-color", "#C8C8C8");
  krydsknapmail.style("color", "#000000");
  krydsknapmail.mousePressed(skift2);
  krydsknapmail.hide();

  //INSTA
  chromeknap = createImg("billeder/chrome.png");
  chromeknap.position(205, 183);
  chromeknap.style("width", "40px");
  chromeknap.mousePressed(visInstaFeed);
  chromeknap.hide();

  krydsKnapInsta = createImg("billeder/krydschrome.png")
  krydsKnapInsta.position(310, 100);
  krydsKnapInsta.style("width", "50px");
  krydsKnapInsta.style("height", "20px");
  krydsKnapInsta.mousePressed(skift5);
  krydsKnapInsta.hide();

  //KALENDER
  kalenderknap = createImg("billeder/kalender.png");
  kalenderknap.position(60, 300);
  kalenderknap.style("width", "40px");
  kalenderknap.mousePressed(visKalender);
  kalenderknap.hide();

  kalenderbilled = createImg("billeder/kalenderplan.png");
  kalenderbilled.position(550, 150);
  kalenderbilled.style("width", "700px");
  kalenderbilled.hide();

  krydsknapkalender = createButton("X");
  krydsknapkalender.position(550, 150);
  krydsknapkalender.size(40, 30);
  krydsknapkalender.style("background-color", "#C8C8C8");
  krydsknapkalender.style("color", "#000000");
  krydsknapkalender.mousePressed(skift3);
  krydsknapkalender.hide();

  //BRUGERNAVN SIDSTE STEP
  brugernavn = createImg("billeder/brugernavn.png");
  brugernavn.position(500, 300);
  brugernavn.style("width", "500px");
  brugernavn.style("height", "200px");
  brugernavn.hide();

  donebrugernavn = createButton("Done!");
  donebrugernavn.position(850, 450);
  donebrugernavn.size(60, 30);
  donebrugernavn.style("background-color", "#C8C8C8");
  donebrugernavn.style("color", "#000000");
  donebrugernavn.mousePressed(Hacked);
  donebrugernavn.hide();

  skrivbruger = createInput(" ");
  skrivbruger.size(240)
  skrivbruger.position(560, 380);
  skrivbruger.hide();

  skrivadgang = createInput("");
  skrivadgang.size(240)
  skrivadgang.position(560, 450);
  skrivadgang.hide();
  //Sætter Tid, sådan at funktionen visBrugernavn vil blive vist efter 45000 millisekunder = 45 sek (javaskript funkktion)
  setTimeout(visBrugernavn, 45000);

  //WIFI   
  wifiknap = createImg("billeder/wifi.png");
  wifiknap.position(width / 1.26315789, height / 1.05109489);
  wifiknap.style("width", "30px");
  wifiknap.style("height", "25px");
  wifiknap.mousePressed(visNetvaerk);
  wifiknap.hide();

  wifi2knap = createImg("billeder/privatwifi.png");
  wifi2knap.position(width / 1.44, height / 1.4112574);
  wifi2knap.style("width", "300px");
  wifi2knap.style("height", "60px");
  wifi2knap.style("background-color", "#566573");
  wifi2knap.style("color", "#F8F9F9");
  /*instaStart er sat til false i de globale variabler, når der trykkes på wifiKnap2 ændres instaStart til true, som 
  vil starte den if-statement, som er længere nede se linje (376-385)*/
  wifi2knap.mousePressed(() => instaStart = true);
  wifi2knap.mouseReleased(skjulUdraab); //mousePressed kan ikke kontrollere 2 udfald, så derfor benytter vi os af mouseReleased også
  wifi2knap.hide();

  wifi1knap = createImg("billeder/offentlig1.png");
  wifi1knap.position(width / 1.44, height / 1.30229);
  wifi1knap.style("width", "300px");
  wifi1knap.style("height", "60px");
  wifi1knap.style("background-color", "#566573");
  wifi1knap.style("color", "#F8F9F9");
  wifi1knap.mousePressed(Hacked);
  wifi1knap.hide();

  wifi3knap = createImg("billeder/offentlig2.png");
  wifi3knap.position(width / 1.44, height / 1.17542028);
  wifi3knap.style("width", "300px");
  wifi3knap.style("height", "60px");
  wifi3knap.style("background-color", "#566573")
  wifi3knap.style("color", "#F8F9F9");
  wifi3knap.mousePressed(Hacked);
  wifi3knap.hide();

  krydsknapwifi = createButton("X");
  krydsknapwifi.position(width / 1.44, height / 1.45009);
  krydsknapwifi.size(300, 20);
  krydsknapwifi.style("background-color", "#566573");
  krydsknapwifi.style("color", "#F8F9F9");
  krydsknapwifi.mousePressed(skift4); //gemmer alt der har med wifi at gøre 
  krydsknapwifi.hide();

  //Udråbstegnet 
  udraabsknap = createImg("billeder/udraab.gif");
  udraabsknap.position(width / 1.25819134, height / 1.05191);
  udraabsknap.style("width", "20px");
  udraabsknap.style("height", "28px");
  udraabsknap.mousePressed(visNetvaerk);
  udraabsknap.hide();

  //Hacking billedet 
  hackedknap = createImg("billeder/hacked.gif");
  hackedknap.position(width / 6.5454, 0);
  hackedknap.style("width", "1000px");
  hackedknap.style("height", "740px");
  hackedknap.hide();
}

function draw() {
  /*if-statement, som siger, at hvis start er true (det bliver den, når der er trykket på loginknappen), 
  så kan den hide de forskellige knapper og kalde på de forskellige funktioner*/
  if (start === true) {
    knap.hide();
    skriv.hide();
    skriv2.hide();
    startSkaerm();
    billedmappe();
  }

  /* Her tildeler den loginNavn til værdierne fra skriv-inputtet, og derefter giver den skrivBruger input værdien fra loginNavn
  dette er indsat i draft, sådan den hele tiden vil opdatere værdierne fra skriv inputtet */
  let loginNavn = skriv.value();
  skrivbruger.value(loginNavn);
}

/*Gør sådan at man skal skrive noget i brugernavn og adgangskode inputs før man kan logge ind
den siger, at hvis de to forskellige input ikke er lig med " " (tomt), 
så skal den sætte start til true, sådan den starter startfunktionen 
!== dette tegn er ikke ligmed*/
function loginSkærm() {
  if (skriv.value() !== " " && skriv2.value() !== " ") {
    start = true;
  }
}

//Startskærm er hjemmeskærmen 
function startSkaerm() {
  background(startskaerm);
  //Baren i bunden bliver lavet med firkanter (rektangler) + text
  fill(170);
  noStroke();
  rect(0, height - 40, width, 50);
  stroke(0);
  rect(0, height - 40, 80, 50);
  fill(0);
  text('START', 5, height - 27, 10);

  //Her indsættes de forskellige ikoner, som ikke er DOM-elementer, men bare billeder + teksten til det som er knapper
  fill(255);
  textSize(13);
  text('Internet Explorer', 35, 120);

  image(indstillinger, 50, 175, 60, 55);
  text('Indstillinger', 48, 250);

  text('Kalender', 52, 375);

  image(bin, 50, 425, 55, 55);
  text('Papirkurv', 50, 495);

  image(mappe, 50, 550, 55, 55);
  text('Dokumenter', 43, 620);

  text('Billeder', 210, 120);

  text('Google Chrome', 190, 250);

  text('Mail', 215, 375);

  clock();

  //Her vises ikonerne, som er knapper
  billedmappeknap.show();
  internetknap.show();
  chromeknap.show();
  mailknap1.show();
  mailknap2.show();
  kalenderknap.show();
  udraabsknap.show();
  wifiknap.show();

  //får kameraet til at lyse grønt, så det ligner man bliver overvåget
  //Video kan kun lade sig gøre, fordi biblioteket er tilføjet i indexfilen 
  capture = createCapture(VIDEO); //Tildele video input objektet
  capture.hide(); //gem webcam feed’et
}

//Funktion som bliver kaldt på, når der bliver trykket på mappe ikonet
function visbilleder() {
  billedmappen.show();
  krydsknapbilleder.show();
}

//Vis Insta billede hvis der er blevet valgt et wifi, hvis ikke, vis ingen internet
function visInstaFeed() {
  if (instaStart === true) {
    krydsKnapInsta.show();
    image(instagram, 300, 100, 800, 500);

  } else { //hvis instaStart stadig er false (internet er ikke valgt) vil siden med ingen internet vises hvis der trykkes på chrome
    krydsKnapInsta.show();
    image(ingenInternetInsta, 295, 97, 800, 500);
  }
}

//Funktion som bliver kaldt på, når der bliver trykket på mail ikonet
function visMail() {
  mailbilled.show();
  scamMail.show();
  krydsknapmail.show();
  notiknap.hide();
  notiknap2.hide();
}

//Funktion som bliver kaldt på, når der bliver trykket på kalender ikonet
function visKalender() {
  kalenderbilled.show();
  krydsknapkalender.show();
}
//Funktion som bliver kaldt på, når der bliver trykket på wifi ikonet
function visNetvaerk() {
  krydsknapwifi.show();
  wifi1knap.show();
  wifi2knap.show();
  wifi3knap.show();
}

//Billeder skift - får indholdet i billedmappen til at forsvinde når der trykkes på kryds 
function skift() {
  krydsknapbilleder.hide();
  billedmappen.hide();

}

//Mail skift - får indholdet i mail til at forsvinde når der trykkes på kryds 
function skift2() {
  krydsknapmail.hide();
  mailbilled.hide();
  scamMail.hide();
}

//Kalender skift - forsvinder når man trykker på kryds
function skift3() {
  krydsknapkalender.hide();
  kalenderbilled.hide();
}

//netvaerk skift - forsvinder når man trykker krydsknappen
function skift4() {
  krydsknapwifi.hide();
  wifi1knap.hide();
  wifi2knap.hide();
  wifi3knap.hide();
}

//skjuler udråbstegnet
function skjulUdraab() {
  udraabsknap.hide()
  wifi1knap.hide();
  wifi2knap.hide();
  wifi3knap.hide();
  krydsknapwifi.hide();
}

//insta skift - får indholdet i insta til at forsvinde når der trykkes på kryds 
function skift5() {
  krydsKnapInsta.hide();
  startSkaerm();
}

//Bitcoin skift - får pop-uppen til at forsvinde når der trykkes på kryds 
function skift6() {
  krydsknapbitcoin.hide();
  bitcoinbesked.hide();

}

//funktion som kommer frem, når 35  sekunder er gået via setTimeout
function visBitcoins() {
  bitcoinbesked.show();
  krydsknapbitcoin.show();
}

//funktion som kommer frem, når 45 sekunder er gået
function visBrugernavn() {
  brugernavn.show();
  skrivbruger.show();
  skrivadgang.show();
  donebrugernavn.show();
}

//funktion som kommer frem, når 27 sekunder er gået
function notifikationMail() {
  notiknap.show();
  notiknap2.show();
}

//Funktionen bruges til når brugeren klikker på de forkerte ting og hacking billedet kommer frem
function Hacked() {
  hackedknap.show();
}

//funktion som tilpasser tiden i programmet til den virkelige tid
function clock() {
  fill(0);
  textSize(20);
  //hour er en funktion som tager den pågældende time (0-23)
  let Hour = hour();
  //minute er en funktion som tager det pågældende minut (0-59) 
  let min = minute();
  //Her er et if-statement, som siger at hvis minutterne er under 10 min, fx 5 min, så vil den hedde 0+min, altså 0+5 = 05 når det står som tekst
  if (min < 10)
    min = "0" + min

  //Day er en funktion som tager den pågældende day (0-31)
  let Day = day();
  //Month er en funktion som tager det pågældende måned (0-12) 
  let Month = month();
  //Year er en funktion som tager det pågældende år
  let Year = year()
  //Her er et if-statement, som siger at hvis day er under 10, fx 5 maj, så vil den hedde 0+5, altså 0+5 = 05 i tekst
  if (Day < 10)
    Day = "0" + Day
  //Samme som med Day
  if (Month < 10)
    Month = "0" + Month

  //Her indsættes hele teksten med dag+måned+år+ mellemrum + time+minut + sekund
  text(Day + "." + Month + "." + Year + "   " + Hour + ":" + min, width / 1.2, height / 1.01986);
}
let tilfældig;

function setup() {
  // Størrelsen på canvas
  createCanvas(windowWidth, windowHeight);
  frameRate(5);
}
function draw() {
  background(255);
  noStroke();
  rectMode(CENTER);

  for (let x = 20; x < windowWidth; x += 40) {

    for (let y = 20; y < windowHeight; y += 40) {

      tilfældig = random(0, 1);

      if (tilfældig > 0.5 && tilfældig < 0.7) {
        fill(random(158, 100), random(100, 150), random(100, 120));
        circle(x, y, 20, 40);

      } else if (tilfældig < 0.5) {
        fill(random(100, 150), random(20, 100), random(0, 120));
        rect(x, y, 30, 30);

      } else if (tilfældig > 0.5) {
        fill(random(58, 100), random(78, 150), random(100, 120));
        ellipse(x, y, 20, 30);
      }
    }
    if (mouseIsPressed) {
      noLoop();
    }

  }
}
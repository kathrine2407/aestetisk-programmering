# MiniX5


[Klik herpå for at se min generative kunst](https://kathrine2407.gitlab.io/aestetisk-programmering/MiniX5/index.html)


![](Skærmbillede_2023-03-19_kl._20.01.30.png)


[Selve koden](https://gitlab.com/kathrine2407/aestetisk-programmering/-/blob/main/MiniX5/MiniX5.js)


### Beskrivelse af programmet samt tilvalg og fravalg


Emnet for denne MiniX gik på at man skulle generere et stykke kode, hvor regler inkorporeres, som videreføre i et uventede og eller komplekse resultater. I starten havde jeg svært ved at forstå hvad hele pointen var med dette emne og helt grundlæggende hvad det vil sige at udføre en generativ kode. Men eftersom jeg så flere og flere eksempler på en generativ kode, gik det op for mig, det at generere  sådan en kode handler om at få så mange forskellige outputs som overhovedet muligt, dog med nogle inkorporerede regler. Hvor det interessante og spændende ville være at kunne generere et stykke kode med så få regler som muligt men alligevel give mange forskellige outputs. Det at computeren selv kreere forskellige kunstneriske udtryk ud fra få regler finder jeg meget opsigtsvækkende. 


Focusing on rules and instructions is not something only programmers do, but also something you do when following a knitting/weaving pattern5 or recipe (as we will see with the preparation of tofu in the next chapter). Artists have also produced instruction-based works of art, as is the case of the Fluxus and Conceptual Art movements of the 1960s and 1970s that set out to challenge art’s object-ness, and encourage its “dematerialization.”


Vi bruger vores digitale enheder til næsten alt i vores hverdag samt arbejdsliv og nu hvor det også kan bruges til kunst, er vildt at tænke på - Vil erhvervet malerkunst dø ud? Og hvad er formålet med at computeren kan programmeres til næsten alt man ønsker? Optimere hverdagen eller ændre erhvervslivet?


Det at få computeren til at udtrykke kunst med så få regler som muligt har jeg forsøgt med min generative "kunst". Min første tanke med min MiniX5 var, at det skulle være noget æstetisk generativt flot kunst. Som tidligere nævnt havde jeg brugt tid på at få inspiration fra andre koder og jeg kom hurtigt frem til at det nok ville kræve mere hvis jeg skulle sætte mig ind i deres koder og kunne forstå dem og dernæst anvende dem selv - fremfor at starte blank. Så jeg startede med at lave et tomt canvas med målene windowWidth og windowHeigt. Dernæst ville jeg gøre brug af for-loop for første gang, med det formål at kunne forstå hvordan syntaksen operere. Først ville jeg brug for-loopet vi havde lært til en forelæsning, hvor et vis antal cirkler laves både ud af x og y aksen. Dernæst ønskede jeg at cirklerne skulle skifte farve ud fra random syntaksen. Eftersom jeg blev bidt af måden for-loop operere, inkorporerede jeg både rektangler og ellipser til “kunstværket”. Da jeg ikke synes det var nok at figurerne ændrede farve, ønskede jeg også at de skulle placeres tilfældigt på canvaset ud fra en variabel, som så skulle forekomme for hvert frame der går (altså over tid). Så i selve min kode indgår random syntaksen flere forskellige steder, både ved figurernes farve og placering og på den måde genere computeren uforudsete kunstværker og den er hermed generativ. 


Hvis man ser på selve koden og de regler jeg har inkorporerede i mit værk, har jeg formået (vil jeg selv sige) at benytte få regler og alligevel ende med mange forskellige outputs med samme kode over tid (framerate). Den første regel, som indgår er det yderste for-loop:


for(let x = 20; x < windowWidth; x+= 40){


Her siger jeg at startpositionen for mine figurer er x=20 og at figurerne skal fortsætte ud til bredden på den given canvas (windowWidth). Dernæst skal mellemrummet mellem hver tegnet figur være x=40, fra deres center. På samme vis gælder det fra y-aksen. Dertil har jeg defineret en variablen tilfældig som er lidt med random 0.1. Som siger noget om hvor figurerne placeres forskelligt på skærmen ud fra random syntaksen. 
Det vil altså sige at hver figur er placeret inden for 0.0 - 0.1 i random spekteret. Hvis man for eksempel ser cirklerne bliver de tegnet hvis tilfældigheden er større end 0.5 og mindre end 0.7, på samme måde foregår det med de to andre figurer. 

Udover at have gjort brug af random syntaksen til placeringerne har jeg anvendt den for farvekoderne, hvor den randomisere R,G,B forskelligt fra figurerne. For eksempel er rektanglerne i en rød randomiseret kode og cirklerne og ellipserne er grå og grønne. 

if (tilfældig > 0.5 && tilfældig < 0.7 ){

fill(random(158,100),random(100,150),random(100,120));

circle(x,y,20,40);

For at stoppe det generative kunstværk og få et enkeltbillede af hvordan et random billede kan se ud, har jeg defineret en if statement som siger at hvis mouse is pressed skal loop funktionen stoppes og et enkelt kunstværk kan ses. 

Det at jeg har givet programmet få regler og computeren selv outputter forskellige placeringer for figurerne resultere i at der bliver skabt et generativt (uforudset) kunstværk for hvert frame. Det at skulle finde på regler selv og computeren giver et uforudset output har været med til at stærke min forståelse for "auto-generator". 

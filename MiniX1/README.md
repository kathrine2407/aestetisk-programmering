# MiniX1

[Klik herpå for at se min MiniX1](https://kathrine2407.gitlab.io/aestetisk-programmering/MiniX1/index.html)

![](Billede1.png)

[Selve koden](https://gitlab.com/kathrine2407/aestetisk-programmering/-/blob/main/MiniX1/MiniX1sketch.js)

# Beskrivelse af programmet 
For at starte med at programmere noget, skal man først finde på noget og netop dette var min første udfordring. Jeg brugte en del tid på at finde frem til hvad mit mål med min miniX1 var. Efter en masse forsøg med forskellige figurere, kom jeg frem til at det kunne være interessant at programmere noget mange af os har kendskab til. Med det sagt begyndte jeg at lave en havbund, hvor farven blå for vandet skulle bruges og sand/beige for havbunden. 
Idet outputtet finder sted på min google chromebrowser og jeg ikke har kendskab til skærmstørrelsen gøre jeg brug af (windowWidth, windowHeight), som min canvas. Det er en systemvariabel, som gemmer bredden og højden af det indre vindue. 

Udover vandet og havbunden har jeg forsøgt mig på at lave tang i det venstre hjørne. Dette har jeg gjort ved brugen af en masse cirkler sat sammen, med forskellige koordinater. 
Eftersom det er en havbund følte jeg det var en nødvendighed at fiskene skulle kunne svømme. Måden hvorpå figurerne jeg har anvendt til at danne fiskene, får bevægelse har jeg gjort brug af let funktionen hvor jeg definere variablen x = 0, hvor x er defineret som bevæg og 0 er lig start positionen. Det vil sige at for hvert x koordinat for figurerne skal der lægges/trækkes bevæg til eller fra, afhængigt af den ønskede retning. Idet jeg har et ønske om at fiskene skal svømme i hver sin retning har de forskellige fortegn.


# En interessant oplevelse
Med udarbejdelse af min første MiniX, har jeg fået en forståelse for hvor lang tid man kan bruge på at lave noget så ”simpelt” som en havbund. Det handler om hvor mange detaljer der ønske at opnås og hvilke funktioner elementerne skal have. Udover det handler kodning også om at have en forståelse for den bagvedliggende matematik, idet skærmen/output stedet er indrettet som en et koordinatsystem. 

En anden interessant oplevelse jeg stød på var at, når jeg ønskede at tilføje noget nyt i til min havbund skal det stå nederst i kodningen og ikke øverst eftersom, det er rækkefølgen på hvad der overlapper hinanden og synliggøres. 

# Refleksioner omkring programmet 
Da jeg skulle konstruerer mine fisk krævede det flere forskellige figurere, samt forskellige placeringer. I starten gættede jeg mig frem til hvad koordinaterne skulle være så det endte med det rigtige out, ergo en fisk. Efter at have siddet med det i noget tid, har jeg fået en bedre forståelse for hvad de forskellige koder indebære, dog kunne jeg med fordel have læst hvad de forskellige koders punkter referere til. De figurer der ”drillede” mest var trekanterne, idet jeg havde svært ved at forstå hvordan de kunne drejes og en hale samt finne kunne ses. 

I mit program er der gjort brug af forskellige farver og måden jeg har valgt farverne og koden dertil er gennem en referenceliste, hvor jeg blot vælger den farve jeg synes der vil se bedst ud og indtaster den. Jeg har ikke den store kendskab til farveregistret, det eneste jeg ved er at jeg har gjort brug af R,G,B - Rød, Grøn, Blå, hvor hver farve har en skala op til 255 og når man ændre tallene sker der en ændring i farven. Så hvis den lyder på 0,0,0 opnås en sort farve og modsat hvis man den er 255,255,255 som er det optimale opnås den hvide farve. 

Men hvorfor går skalaen kun op til 255 og ikke 700? 

I processen er det gået op for mig hvor individuelt det at kode egentlig er. For hvis andre skulle lave en havbund, havde det nok set helt anderledes ud end min. Det at sidde alene og nørkle med et projekt i stedet for i en gruppe, gør netop at ens kode bliver personificerede og ens egen forståelse for det æstetiske i kodning kommer til udtryk. Vi som individer har hver vores opfattelse af noget det er smuk, grimt og eller sejt. Derudover vil jeg også vove den påstand at sidde med det selv giver en hurtig og bedre forståelse for det grundlæggende i kodning, fremfor hvis man sad flere sammen, fordi man netop selv har hands on på det nuværende projekt. 

# Opgraderingsmuligheder
Efter udarbejdelsen af min MiniX1, kom jeg i tanke om at da jeg var lille spillede jeg det såkaldte fiskespil ”Hungry Fish”, som handler om at en fisk skal spise mindre fisk end den selv for at blive større og blive til en anden fisk. Med det sagt kunne det være fedt at programmere flere fisk og programmer det således at den primære fisk ændres i det den svømmer ind i andre fisk. Dog kræver det nok mere end som så, så en realistisk opgraderingsmulighed kunne være at de to fisk kunne svømme i loop og komme tilbage, uden blot at forsvinde fra skærmen. 

# Referenceliste

[P5,js](https://p5js.org/reference/)

[Farvekoder](https://www.farvernesbetydning.dk/farvekoder-find-html-farvekoder-til-din-hjemmeside/?fbclid=IwAR3KavVDrCH5Q48hbd60FSzdoax5mtKvKo8R397bJVu1Rw7tn8Vp27f_aPc)



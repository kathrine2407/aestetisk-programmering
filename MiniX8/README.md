## MiniX8

#### Flowchart over min miniX4
Med denne minix8 skulle vi genbesøge en af vores tidligere minix'er og lave et flowchart. Jeg var lidt usikker på hvilken minix der var mest kompleks, da de fleste jeg talte med valgte deres udarbejdet spil. Dog følte jeg ikke at mit spil var så givne og kompleks som jeg turde håbe på derfor gik jeg med min minix4. Som indeholder en skitse af eb hjemmeside med forskellige beklædningdele. Måden jeg startede mit flowchart på, var at se på hvilke funktioner min minix indeholdte og dernæst tage det trin for trin. Jeg må dog inderømme at jeg var lidt i tvivl om hvordan opsætningen skulle se ud og hvad man som bruger selv ville udforske først. Så mit endelige flowchart er udarbejdet ud fra hvad jeg selv personligt ville undersøge på en tilfældig hjemmeside først. 

![](Flowchart_over_minix4.png)

### Gruppe
#### Flowchart 1 - “Drink master”

Denne ide fik vi med inspiration fra et gammelt Y8 spil, hvor man kunne blande drinks bag en bag. Dette er egentlig også hvad vores nye ide går ud på, dog bare med færre valgmuligheder og mere enkelt design, så det passer til vores programmeringsskills. Det går ud på, at man gennem spillet får valgmuligheder for hvordan man vil blande sin drink, og alt efter dette vil drinken blive anderledes. Til sidst vil ens færdige drink blive ranket, og man kan starte forfra. 
![](Flowchart_1.png)

#### Flowchart 2 - “Hacking”

Ideen med hacking er med inspiration fra nutidens brug af computer. Hacking er uheldigis et meget aktuelt problem, og det bliver nemmere og nemmere at blive hacket. Derfor ønskede vi at sætte fokus på alle de “skjulte” hacking metoder, hvor man desværre kan risikere at blive fanget. I spillet bliver man derfor præsentere for forskellige hacking metoder, hvorpå man skal forsøge at undgå dem ved at træffe det rette valg. Hvis man træffer det forkerte, vil man ende med at tabe spillet og få alle sine oplysninger stjålet. 
![](Flowchart_2.png)

#### What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?

Vores første step til at finde frem til vores udkast af det endelige projekt gik med brainstorming. Her kom vi på mange forskellige ideer, som både var meget komplekse men også ligetil. Det der skabte udfordringer for os, var netop at vores flowcharts skulle forenkles på bedste vis, så alle kunne få den rigtige forståelse for programmet. 
Eftersom vi ønskede at flowchartet skulle være så brugervenligt som muligt, krævede det at vi beskrev programmet letlæseligt, men også fyldestgørende. For eksempel i vores flowchart 1 “Drink master” har vi gjort brug af ordvalg “valgmuligheder” frem for at benytte ordet “algoritme”, da man ikke med sikkerhed ved om brugeren har kendskab til algoritmer. Med disse flowcharts har vi fået forståelsen for, hvor essentielt det er at gøre brug af alment ordvalg. 

#### What are the technical challenges facing the two ideas and how are you going to address these?


Tekniske udfordringer som kunne forekomme ved den første ide med “drink master” ville være i forhold til det visuelle. Her kan der være udfordringer med at vise at glasset for eksempel bliver fyldt op, eller at drinken rystes. 
I forhold til den anden ide med hacking er der nogle tekniske udfordringer i forhold til at få de forskellige scenarier op på dette rette tidspunkt - hvordan gør vi det synligt? Hvordan kommer de ind på dette rette tidspunkt, og hvordan sikrer vi os, at brugeren forstår konceptet og udfordringer på bedst vis? 
Løsning til de problemer vil vi tage fat i tidligere lavet miniX, og vores erfaring med forskellige syntakser og programmer. Derudover er begge ideer nogle, som tager fat i emner såsom data capture, objekt programmering med mere, hvor vi ligeledes kan hente inspiration fra andre klassekammerater, som måske har siddet med lignede problemstillinger indenfor emnet. 

#### In which ways are the individual and the group flowcharts you produced useful?

Gruppens flowchart har været god til at fremhæve eventuelle problemer og hvordan man kan fikse dem og komme dem i forkøbet. De er gode til at bruges som en form for plan over hvad de to programmer kan komme til at indeholde, og man kan undervejs brainstorme hvilket der er vigtigt at have med og hvad der kunne være sjovt at programmere. Dog kan det være svært at vide, hvad der er over vores evner at programmere, når man laver flowchartet inden som en plan. De bruges derfor mere som en ide, så det måske er nemmere at overskue, når man skal i gang med programmeringen, men intet ved flowchartet er noget vi kommer til at bruge med sikkerhed. 
I det individuelle flowchart bruges det mere som en oversigt over hvordan programmet virker, da det endelige program allerede er lavet færdigt. Det kan bruges til at forklare både indviklede og mindre indviklede programmer på en måde, hvor alle forstår hvordan de fungerer på. 

#### Gruppe 1 GITLAB

[Camillas gitlab](https://gitlab.com/cami601k/aestetiskprogrammering)

[Thea Heskjærs gitlab](https://gitlab.com/thea.heskjaer/aestetisk-programmering)

[Rikkes gitlab](https://gitlab.com/RikkeOsmann/aesthetic-programming)

[Thea Uhds gitlab](https://gitlab.com/theauhd/aestetiskprogrammering)



#### Reference 
https://www.y8.com/games/bartender_the_right_mix






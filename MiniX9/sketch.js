
let Camillahoved;
let andreHoved;
let insideThoughts = []; 
let outsideThoughts = [];


function preload() {
  //Her loades begge JSON filer ind og tildeles et nyt navn
  Camillahoved = loadJSON('myHead.json');
  andreHoved = loadJSON('outside.json');

  //Her loades billedefilen ind og tildeles et navn
  mand = loadImage('mand.jpg');
}
//function som laves til at gøre de indvendige tanker synlige 
function indvendigSynlig() {
 
  //Her tildeles "tankeMylder", camillahoved (Jsonfilen). arrayet i myhead JSON-filen 
  tankeMylder = Camillahoved.mithoved;
 
  //Her bestemmes der hvor mange tanker som skal være på skærmen - mellem 2 og 4 tanker dukker op
  let addTanker = random(2,4);

  //Derefter laves der et forloop, hvor "i" først defineres og tildeles 0, og fortæller at i skal starte med at tælle fra 0. 
  //Derefter skal i være mindre end addTanker, som er et random tal mellem 2 og 4, det vil sige, at i tæller op til tallet. 
  //Afslutning bliver der lagt 1 til i for hver gang loopet kører.
  for (let i = 0; i <= addTanker; i++) {
   
    //Her defineres egnetanker, som bliver den valgte tanke fra arrayet. Her tager den et random tal fra arrayet længde i JSON .
    // Lokal variabel 
    // int gør at det er et ligetal
    //length er antallet af de mange tanker der er i egnetanker i arrayet - json filen
    //tæller altid fra 0
    let egneTanker = int(random(tankeMylder.length));
    //Derefter putter den en ny i arrayet i sketchfilen
    //egneTanker finder et tal - som svare til at hver sætning har et tal i arrayet
    //Push = noget nyt ind i array
        insideThoughts.push(new notNew(tankeMylder[egneTanker].myhead));
  
  }
}

function udvendigSynlig() {
 //Her tildeles "snak", andreHoved (Jsonfilen). arrayet i filen
  snak = andreHoved.udenfor;
  //Her bestemmes der hvor mange tanker som skal være på skærmen
  let tilføjTanker = 6;

   //Derefter laves der et forloop, hvor indeks først defineres og tildeles 0, og fortæller at indeks skal starte med at tælle fra 0. 
   //Derefter skal indeks være mindre end tilføjtanker, som er 3, det vil sige, at indeks tæller op til tallet. 
   //Afslutning bliver der lagt 1 til indeks for hver gang loopet kører.
   //sp et forloop der tjekker om den er mindre end 6 og hvis den er bliver der lige pushes en tekst mere 

  for (let j = 0; j <= tilføjTanker; j++) {
    //Her defineres andretanker, som bliver den valgte tanke fra arrayet. Her tager den et random tal fra arrayet længde i JSON .
    // int betyder det skal være hele tal
    let andreTanker = int(random(snak.length));
    
      //Derefter putter den en ny i arrayet i sketchfilen
     //egneTanker finder et tal - som svare til at hver sætning har et tal i arrayet
     //Push = noget nyt ind i array
        outsideThoughts.push(new ikkeNy(snak[andreTanker].udtryk));

  }
}

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(255);
  //Indsættelse af billede 
    imageMode(CENTER);
    image(mand, width / 2, height / 2);

   //Den kalder på de classes (worldwide og act) 
  //og fortæller den at de skal virke på InsideThoughts 
  //badWords 
  // worldWide 
  //act beskriver hvordan teksten skal være 
  for (let badWords in insideThoughts) {
    insideThoughts[badWords].act1();
    insideThoughts[badWords].looks1();

    //Når den så bliver showed (notFalse altså når den er true), så skal den så fjerne den igen når den når toppen
    //splice = fjernet fra arrayet - så den kan sættes ind igen
    let support = insideThoughts[badWords].shows1();
    if (support == "forsvindFraSkærm") {
      insideThoughts.splice(badWords);

   }
  }

   //samme som før bare med outside 
   for (let goodWords in outsideThoughts) {
    outsideThoughts[goodWords].act2();
    outsideThoughts[goodWords].looks2();

    let hjælp = outsideThoughts[goodWords].shows2();
    if (hjælp == "forsvindFraSkærm") {
      outsideThoughts.splice(goodWords);

   }
  }

  //Denne if-statement tjekker hvor mange tanker der er tilbage på skærmen, hvis der er mindre end 4
  //så kalder den på funktionen, som genererer nogle nye og kalder på indvendigSynlig funktionen
  if (insideThoughts.length <= 4) {
    indvendigSynlig();
  }
  //hvis der er mindre end 2 tilbage på skærmen skal den gå tilbage til for loopet udvendigsynlig og hvis vi siger der skal komme 6 nye
  if (outsideThoughts.length <= 2) {
   udvendigSynlig();
  }

}

//En form for class for selve teksten
//Styling af teksterne 
function notNew(fåTanker) {
  //attributes of text
  //Tager en random værdi mellem 20 og 25)
  this.size = random(20, 25);
  this.speed = random(1, 3);
  this.y = random(height/3.0, height+50);
  this.x = width/2;
  
  this.act1 = function() {
   this.y-= this.speed; //for at teksterne både går op 
  
  };
  this.looks1 = function() {
    textSize(this.size);
    textAlign(CENTER);
    //Ingen streg rundt om teksten
    noStroke();
    fill(250);
    //Her tildeles de notNew og arrayet med positionen som er defineret længere oppe
    text(fåTanker, this.x, this.y);
  };
  //Først definerer den status, bagefter siger den, hvis y postionen er mindre end 4 og større end højden + 10, 
  //så skal den sætte status til "forsvinderfraskærmen"
  this.shows1 = function() {
    let status;
    if (this.y <= 4 || this.y >= height+10){
      status = "forsvindFraSkærm";
    }
    return status;
  };
}

function ikkeNy(snakOmDig) {
  //attributes of text
  //Tager en random værdi mellem 20 og 25)
  this.size = random(14, 20);

  this.speed = random(1, 3);
  this.y = random(height/3.0, height+5);
  //vælger random mellem de to værdier
  this.x= random([200, 1200]); //klammen betyder at den skal den komme på 200 eller 1200 random
  
  this.act2 = function() {
   this.y-= this.speed;
  
  };
  this.looks2 = function() {
    textSize(this.size);
    textAlign(CENTER);
    //Ingen streg rundt om teksten
    noStroke();
    fill(250,0,0);
    text(snakOmDig, this.x, this.y);
  };
  //check disappeared objects
 this.shows2 = function() {
    let status;
  if (this.y <= 4.34387 || this.y >= height+10.34387){
     status = "forsvindFraSkærm";
   }
    return status;
  };
}